<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'db' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Sq2Q&yhnprKTjhj MF3R<urV~vyCj#udR1.|0u .6aFq>Su(E).J:xXv/o1J_Q[@' );
define( 'SECURE_AUTH_KEY',  'Z/t3+z?,$0kB1FRUknGY~w>. Gzw+@aU9?|{+I!1>2h[K(^A,f29WjE1lbF>T<T;' );
define( 'LOGGED_IN_KEY',    '}~/^b6Ux30!p}S0(nL4Nyab+4!@ @/>2r:%`pBpZ`z~M]M#oGSFRzd13t!:Rj<Jj' );
define( 'NONCE_KEY',        'Mv@F&&AF:&gqGnK+D8%Wl%&5h7#Ve`]IK_qkL2GJ!`j1FJvjP|/n|[L?F@+_H6$z' );
define( 'AUTH_SALT',        'L8PnR7?-QCL!5+&W9ChtmU[Rk};zAC{^WxzA!>NGAZt^&&{I7Lnl]9:f_nwjr{2L' );
define( 'SECURE_AUTH_SALT', 'yzZM2izIZs7}/gP5f.:eU0hff=E8rsS5@R7]QcO9*>{tnK.(vU/{zamj(:6FjUoC' );
define( 'LOGGED_IN_SALT',   'r!Jih=(UJvF*VfB+k.YF3/.|)pW|<pM.aV4=;L(=~D6X%xr^r1I[p1^[!V|?y#A_' );
define( 'NONCE_SALT',       '7n5h_FBWl>&>NuGXu]qEq(C8|A=Dy5Bn6(sn_VO%3FUY+)qg.pVwM#:4o3?Mpzc]' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
